$(".slick-slider-1").slick({
  // normal options...
  infinite: false,
  slidesToShow: 3,
  lazyLoad: "ondemand",
  easing: "ease-in-out",
  arrows: true,
  infinite: true,
  prevArrow:
    '<button type="button" class="slick-prev"><img src="assets/img/s1.png"></button>',
  nextArrow:
    '<button type="button" class="slick-next"><img src="assets/img/s1.png"></button>',

  // the magic
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        infinite: true
      }
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});

$(".case-slider").slick({
  // normal options...
  infinite: false,
  slidesToShow: 1,
  lazyLoad: "ondemand",
  easing: "ease-in-out",
  arrows: true,
  infinite: true,
  dots: true,
  prevArrow:
    '<button type="button" class="slick-prev"><img src="assets/img/s2.png"></button>',
  nextArrow:
    '<button type="button" class="slick-next"><img src="assets/img/s2.png"></button>',

  // the magic
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        infinite: true
      }
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
$(".calaps_moduls").on("show.bs.collapse", function() {
  $(this)
    .parent()
    .addClass("calaps_shadow");
}),
  $(".calaps_moduls").on("hide.bs.collapse", function() {
    $(this)
      .parent()
      .removeClass("calaps_shadow");
  });
